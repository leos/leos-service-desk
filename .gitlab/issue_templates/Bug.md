------
BUG

1. Description
---Please provide a short description of the bug---

2. Steps to reproduce
---Please describe the steps to reproduce the behaviour--

3. LEOS version 
---Please provide the LEOS version used---

4. Browser used
---Please provide details regarding the browser used---

5. Other remarks


------
**If you would like to report a security vulnerability, please follow the Vulnerability Disclosure Policy described here:**
https://commission.europa.eu/legal-notice/vulnerability-disclosure-policy_en 

**If your ticket contains sensitive information, don't forget to check the box:**
"This issue is confidential and should only be visible to team members with at least the Planner role."


