REQUEST FOR INFORMATION

-----------------------

Please let us know your questions or what additional information you would need.
* If there are questions about already published content, please add the relevant references.
* If your ticket contains sensitive information, don't forget to check the box:
"This issue is confidential and should only be visible to team members with at least the Planner role."


-----------------------

Looking for information to get started with LEOS? Please find below a few useful information:
* LEOS starter pack:https://interoperable-europe.ec.europa.eu/node/705219.
* LEOS framework: https://code.europa.eu/groups/leos/-/wikis/LEOS-Framework  
