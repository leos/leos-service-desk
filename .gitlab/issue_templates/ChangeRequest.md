------

**CHANGE REQUEST**

You have selected to create a Change Request. 
Please fill in the information below.

1.	Current Situation:
Please describe the current behaviour of the software for which you require a change.

2.	Desired Situation:
Please describe in as many details as possible the desired behaviour of the software. 

3.	Business case:
Please describe the impact and/or benefits of implementing this request.

4.	Priority:
Please provide the priority you associate from your perspective to this request.
A numeric value denoting the priority of the change. The possible values are: 5=Very high, 4=High, 3=Medium, 2=Low, 1=Very low

5.	Out of Scope:
Please provide further details about the scope of this change request, highlighting the elements that would be out of scope. 

6.	Other remarks

------

NOTE:  The full Change Management process is described here: https://code.europa.eu/groups/leos/-/wikis/How-to-contribute/Change-Request

------
**If your ticket contains sensitive information, don't forget to check the box:**
"This issue is confidential and should only be visible to team members with at least the Planner role."




